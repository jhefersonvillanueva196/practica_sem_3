using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private int estado = 0;
    private int contador = 0;
    private float velocidad = 10f;

    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        estado = 0;
        animator.SetInteger("Estado", estado);
        rb.velocity = new Vector2(velocidad, rb.velocity.y);

        if (Input.GetKey(KeyCode.DownArrow))
        {
            estado = 2;
            animator.SetInteger("Estado", estado);
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            estado = 1;
            animator.SetInteger("Estado", estado);
            rb.velocity = new Vector2(velocidad, 45);
        }

        if(contador == 10)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemigo" && estado == 2)
        {
            Destroy(collision.gameObject);
            velocidad += 0.5f;
            contador++;
        }
        
        if(collision.gameObject.tag == "Enemigo" && estado == 0)
        {
            animator.SetInteger("Estado", 3);
            Destroy(this);
        }
    }
}
